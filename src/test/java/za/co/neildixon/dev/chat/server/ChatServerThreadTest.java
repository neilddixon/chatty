package za.co.neildixon.dev.chat.server;

import org.junit.Test;

import java.net.Socket;

import static org.junit.Assert.*;

public class ChatServerThreadTest {

    static final int DEFAULT_PORT = 2000;

    @Test
    public void chatServerRuns() throws Exception {
        ChatServerThread chatServerThread = new ChatServerThread("127.0.0.1", DEFAULT_PORT);
        chatServerThread.start();

        Thread.sleep(2000);
        assertTrue(chatServerThread.isAlive());
        chatServerThread.interrupt();
        chatServerThread.join();
        assertFalse(chatServerThread.isAlive());

    }

    @Test
    public void chatServerRunsAndRegistersClientHandlerThread() throws Exception {
        ChatServerThread chatServerThread = new ChatServerThread("127.0.0.1", DEFAULT_PORT + 1);
        chatServerThread.start();

        Thread.sleep(2000);
        try (Socket socket = new Socket("127.0.0.1", DEFAULT_PORT + 1)) {
            assertTrue(socket.isBound());

            Thread.sleep(1000);

            assertEquals(1, chatServerThread.getClientHandlerThreads().size());

            chatServerThread.interrupt();
            chatServerThread.join();
        }
    }

}