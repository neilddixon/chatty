package za.co.neildixon.dev.chat.client;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import za.co.neildixon.dev.chat.server.ChatServerThread;

import java.util.Scanner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ChatClientThreadTest {

    static final int DEFAULT_PORT = 2000;

    ChatServerThread chatServerThread;

    @Before
    public void beforeTest() {
        this.chatServerThread = new ChatServerThread("127.0.0.1", DEFAULT_PORT);
        try {
            this.chatServerThread.start();
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void afterTest() {
        this.chatServerThread.interrupt();
        try {
            this.chatServerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void chatClientConnectsToServerAndRuns() throws Exception {
        String inputText = "bob\nhello\n";
        Scanner inputScanner = new Scanner(inputText);
        ChatClientThread chatClientThread = new ChatClientThread("127.0.0.1", DEFAULT_PORT, inputScanner);
        chatClientThread.start();

        assertTrue(chatClientThread.isAlive());

        Thread.sleep(2000);

        chatClientThread.interrupt();
        chatClientThread.join();
        assertFalse(chatClientThread.isAlive());

    }

}