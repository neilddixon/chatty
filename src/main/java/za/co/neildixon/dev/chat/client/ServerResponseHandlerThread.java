package za.co.neildixon.dev.chat.client;

import za.co.neildixon.dev.chat.common.Message;

import java.io.EOFException;
import java.io.ObjectInputStream;
import java.net.SocketException;


public class ServerResponseHandlerThread extends Thread {

    private ObjectInputStream inputReader;

    public ServerResponseHandlerThread(ObjectInputStream inputReader) {
        this.inputReader = inputReader;
    }

    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Message message = (Message) inputReader.readObject();
                if (message != null) {
                    System.out.println(message.getText());
                    //System.out.print("> ");
                }
            }
            System.out.println(Thread.currentThread().getName() + " is exiting");
        } catch (EOFException | SocketException ignored) {
        }//end of stream, peer closed connection
        catch (Exception e) {
            System.err.println("Error while running ServerResponseHandlerThread:" + e.getMessage());
        }
    }


}
