package za.co.neildixon.dev.chat.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class ChatServerThread extends Thread {
    private final String host;
    private final int port;
    private ServerSocket serverSocketListener;
    private final List<ClientHandlerThread> clientHandlerThreads = new ArrayList<>();

    public ChatServerThread(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void interrupt() {
        terminateServerSocket();
        super.interrupt();
    }

    private void terminateServerSocket() {
        try {
            this.serverSocketListener.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    List<ClientHandlerThread> getClientHandlerThreads() {
        return this.clientHandlerThreads;
    }

    @Override
    public void run() {
        try {
            this.serverSocketListener = new ServerSocket(port, 50, InetAddress.getByName(host));
            System.out.println(String.format("Chat server running, accepting connections at:%s:%s", host, port));
            try {
                while (!isInterrupted()) {
                    ClientHandlerThread clientHandlerThread = new ClientHandlerThread(serverSocketListener.accept());
                    this.clientHandlerThreads.add(clientHandlerThread);
                    clientHandlerThread.start();
                    System.out.println(String.format("Registered client handler thread %s", clientHandlerThread.getName()));
                }

            } catch (IOException e) {
                //e.printStackTrace();
            } finally {
                System.out.println("Shutting down client handler threads...");
                clientHandlerThreads.forEach(Thread::interrupt);
                System.out.println("Server stopping...");
            }
        } catch (SocketException ignore) {
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
