package za.co.neildixon.dev.chat.server;


public class ChatServer {

    public static void main(String[] args) {

        String host = "localhost";
        int port = 0;

        if (args.length == 2) {
            host = args[0];
            port = Integer.parseInt(args[1]);
        } else {
            System.out.println("Parameters required: hostname port");
            System.exit(1);
        }

        final Thread serverThread = new ChatServerThread(host, port);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutting down...");

            try {
                serverThread.interrupt();
                serverThread.join();
                Thread.sleep(500);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                e.printStackTrace();
            }
        }));
        serverThread.start();

    }


}
