package za.co.neildixon.dev.chat.server;

import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;


public class ChatDB {

    private static volatile Map<String, ObjectOutputStream> users = new HashMap<>(); //username, ObjectOutputStream

    public static synchronized boolean userExists(String username){
        return users.containsKey(username);
    }

    public static synchronized void addUser(String username, ObjectOutputStream clientPrintWriter){
        users.put(username, clientPrintWriter);
    }

    public static synchronized void removeUser(String username){
        users.remove(username);
    }

    public static synchronized int getNumberOfUsers(){
        return users.size();
    }

    public static synchronized  Map<String,ObjectOutputStream> getUsers(){
        return users;
    }
}
