package za.co.neildixon.dev.chat.client;

import za.co.neildixon.dev.chat.common.Message;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChatClientThread extends Thread {
    private static final Logger logger = Logger.getLogger(ChatClientThread.class.getSimpleName());

    private final String serverHost;
    private final int serverPort;
    private Socket socket = null;
    private Scanner inputScanner;
    private ObjectOutputStream outputWriter;
    private ObjectInputStream inputReader;

    public ChatClientThread(String serverHost, int serverPort, Scanner inputScanner) {
        this.serverHost = serverHost;
        this.serverPort = serverPort;
        this.inputScanner = inputScanner;
    }

    @Override
    public void interrupt() {
        terminateClientSocket();
        super.interrupt();
    }

    private void terminateClientSocket() {
        try {
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void establishServerConnection() throws Exception {
        logger.info(String.format("Running client, establishing connection to %s:%s", serverHost, serverPort));
        InetAddress serverAddress = InetAddress.getByName(serverHost);
        socket = new Socket(serverAddress, serverPort);
        outputWriter = new ObjectOutputStream(socket.getOutputStream());//write to server
        inputReader = new ObjectInputStream(socket.getInputStream()); //read from server
    }

    @Override
    public void run() {
        Thread serverResponseHandlerThread = null;
        try {
            establishServerConnection();

            serverResponseHandlerThread = new ServerResponseHandlerThread(inputReader);
            serverResponseHandlerThread.start();
            logger.info("Connected, ready....");

            while (!Thread.currentThread().isInterrupted()) {
                readUserInput();
            }
        } catch (Exception e) {
            logger.log(Level.INFO, "Error occurred", e);
        } finally {
            if (serverResponseHandlerThread != null) {
                serverResponseHandlerThread.interrupt();
            }
            close(outputWriter);
            close(inputReader);
            close(socket);

        }
    }

    private void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void readUserInput() throws IOException {
        while (inputScanner.hasNextLine()) {
            final String line = inputScanner.nextLine();

            if (":quit".equalsIgnoreCase(line)) {
                sendMessage(new Message(Message.MESSAGE_TYPE_QUIT, ""));
                logger.info("Quit, goodbye....");
                Thread.currentThread().interrupt();
                return;
            } else if (":help".equalsIgnoreCase(line)) {
                sendMessage(new Message(Message.MESSAGE_TYPE_HELP, line));
            } else if (":users".equalsIgnoreCase(line)) {
                sendMessage(new Message(Message.MESSAGE_TYPE_USERS, line));
            } else {
                sendMessage(new Message(Message.MESSAGE_TYPE_INFO, line));
            }
        }
    }

    private void sendMessage(Message message) throws IOException {
        outputWriter.writeObject(message);
    }
}
