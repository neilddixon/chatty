package za.co.neildixon.dev.chat.server;


import za.co.neildixon.dev.chat.common.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Thread handles interactions with a single client
 */
public class ClientHandlerThread extends Thread {

    private Socket socket;
    private ObjectInputStream inputStream;  //input stream from client
    private ObjectOutputStream outputStream; //output stream to client
    private String username;


    public ClientHandlerThread(Socket clientSocket) {
        this.socket = clientSocket;
    }

    @Override
    public void run() {
        System.out.println(String.format("ClientHandlerThread:%s  running", Thread.currentThread().getName()));

        try {
            inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream = new ObjectOutputStream(socket.getOutputStream());

            while (!Thread.currentThread().isInterrupted()) {
                if (username == null) {
                    readUsername();
                } else {
                    readUserMessages();
                }
            }


        } catch (Exception e) {
            //e.printStackTrace();

        } finally {
            System.out.println(String.format("ClientHandlerThread:%s  exiting username:", Thread.currentThread().getName(), username));

            ChatDB.removeUser(username);

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                }
            }

            try {
                socket.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Read message from clients input stream
     *
     * @return
     * @throws IOException
     */
    private Message readUserMessage() throws IOException {
        Message message = null;
        try {
            message = (Message) inputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Message class not found", e);
        }
        return message;
    }


    private void readUsername() throws IOException {

        messageUser("To join the chat room, please enter valid chat name");

        System.out.println(String.format("Thread:%s  reading user name", Thread.currentThread().getName()));
        Message message = readUserMessage();

        String name = message.getText();

        if (name == null || name.isEmpty()) {
            messageUser("Invalid username");
            return;
        }

        if (ChatDB.userExists(name)) {
            messageUser("Name already exists, enter another");
        } else {
            ChatDB.addUser(name, outputStream);
            username = name;

            broadcastMessage(String.format("%s has joined the chat room. There are %s users online", username, ChatDB.getNumberOfUsers()));
            messageUser(String.format("Welcome %s, for help type :help", username));
        }
    }

    private void readUserMessages() throws IOException {
            System.out.println(String.format("Thread:%s username:%s  reading user messages", Thread.currentThread().getName(), username));

            Message message = readUserMessage();

            if (message == null)//end of stream
                return;

            if (message.getType() == Message.MESSAGE_TYPE_QUIT) {
                System.out.println(String.format("Thread:%s username:%s  quit, notifying others", Thread.currentThread().getName(), username));
                ChatDB.removeUser(username);
                broadcastMessage(String.format("%s has left the chat room. There are %s users online", username, ChatDB.getNumberOfUsers()));
                Thread.currentThread().interrupt();
                return;
            } else if (message.getType() == Message.MESSAGE_TYPE_HELP) {
                System.out.println(String.format("Thread:%s username:%s  sent help message, replying", Thread.currentThread().getName(), username));
                messageUser("<<Help>> :quit to quit or :users to list users. @username to send message to specific user");
            } else if (message.getType() == Message.MESSAGE_TYPE_USERS) {
                System.out.println(String.format("Thread:%s username:%s  sent users message, replying", Thread.currentThread().getName(), username));
                String users = "";
                for (String username : ChatDB.getUsers().keySet()) {
                    String name = (username.equals(this.username) ? username + " (you)" : username);
                    users += name;
                    users += ",";
                }
                if (users.endsWith(","))
                    users = users.substring(0, users.lastIndexOf(","));
                messageUser("<<Users>> " + users);
            } else {
                String messageText = message.getText();
                if (messageText != null && messageText.startsWith("@")) {
                    String targetUsername = messageText.split(" ")[0].split("@")[1];
                    String messageTextExtract = messageText.split("@" + targetUsername)[1].trim();
                    System.out.println(String.format("Thread:%s username:%s sent message, sending to user: %s", Thread.currentThread().getName(), username, targetUsername));
                    messageUser(targetUsername, username + ":" + messageTextExtract);
                } else {
                    System.out.println(String.format("Thread:%s username:%s sent message, broadcasting", Thread.currentThread().getName(), username));
                    broadcastMessage("[broadcast] " + username + ":" + message.getText());
                }
            }
    }

    /**
     * Send message to this thread's client
     *
     * @param message
     */
    private void messageUser(String message) {
        try {
            outputStream.writeObject(new Message(this.username, message));
        } catch (Exception e) {
            System.err.println("Error sending message to user:" + this.username);
        }
    }

    private void messageUser(String username, String message) {
        Optional<Map.Entry<String, ObjectOutputStream>> user = ChatDB.getUsers().entrySet().stream().filter(e -> e.getKey().equals(username)).findFirst();
        if (user.isPresent()) {
            try {
                user.get().getValue().writeObject(new Message(this.username, message));
            } catch (Exception e) {
                System.err.println("Error sending targeted message to user:" + username);
            }
        }
    }

    /**
     * Broadcast message to all clients
     *
     * @param message
     */
    private void broadcastMessage(String message) {
        List<Map.Entry<String, ObjectOutputStream>> usersExceptCurrent = ChatDB.getUsers()
                .entrySet()
                .stream()
                .filter(e -> !e.getKey().equals(username))
                .collect(Collectors.toList());
        for (Map.Entry<String, ObjectOutputStream> client : usersExceptCurrent) {
            try {
                client.getValue().writeObject(new Message(this.username, message));
            } catch (Exception e) {
                System.err.println("Error sending broadcast message to user:" + client.getKey());
            }

        }
    }
}
