package za.co.neildixon.dev.chat.client;



import java.util.Scanner;
import java.util.logging.Logger;

public class ChatClient {
    private static final Logger logger = Logger.getLogger(ChatClient.class.getSimpleName());

    public static void main(String[] args) {

        String serverhost = "localhost";
        int serverPort = 0;

        if (args.length == 2) {
            serverhost = args[0];
            serverPort = Integer.parseInt(args[1]);
        } else {
            logger.info("Parameters required: hostname port");
            System.exit(1);
        }

        Scanner inputScanner = new Scanner(System.in);
        ChatClientThread chatClientThread = new ChatClientThread(serverhost, serverPort, inputScanner);
        chatClientThread.start();

        try {
            chatClientThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            inputScanner.close();
        }

        System.exit(0);
    }


}
