package za.co.neildixon.dev.chat.common;

import java.io.Serializable;


public class Message implements Serializable {

    private static final long serialVersionUID = 3060054835159993421L;

    public static final int MESSAGE_TYPE_INFO = 0;
    public static final int MESSAGE_TYPE_QUIT = 1;
    public static final int MESSAGE_TYPE_HELP = 2;
    public static final int MESSAGE_TYPE_USERS = 3;

    private int type = MESSAGE_TYPE_INFO;

    private String text;
    private String sender;

    public Message(String sender, String text) {
        this.text = text;
        this.sender = sender;
    }

    public Message(int type, String text) {
        this.type = type;
        this.text = text;
        this.sender = "unknown";
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSender() {
        return this.sender;
    }
}
